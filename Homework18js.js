    const objectOld = {
      $number: 5,
      $string: 'text',
      $boolean: false,
      $nan: NaN,
      $array: [1, 2, 3, 'a'],
      $null: null,
      $undefined: undefined,
      $object: {
        $number1: 5,
        $string1: 'text',
        $boolean1: false,
        $nan1: NaN,
        $array1: [1, 2, 3, 4],
        $null1: null,
        $undefined1: undefined,
        $object1: {
          $number2: 5,
          $string2: 'text',
          $boolean2: false,
          $nan2: NaN,
          $array2: [1, 2, 3, 4],
          $null2: null,
          $undefined2: undefined,
          $object2: {
            $array3: [1, 2, 3, 4],
          },
        },
      },
    }

    function copyArray(array) {
      const tempArray = [];
      for (let i = 0; i < array.length; i++) {
        tempArray[i] = array[i];
      }
      return tempArray;
    }

    function copyObject(objectOld) {
      const emptyObject = {};
      for (let key in objectOld) {
        if (typeof objectOld[key] === 'object' && objectOld[key] !== null) {
          if (Array.isArray(objectOld[key])) {
            emptyObject[key] = copyArray(objectOld[key]);
          } else {
            emptyObject[key] = copyObject(objectOld[key]);
          }
        } else {
            emptyObject[key] = objectOld[key];
        }
      }
      return emptyObject;
    }

    const newObject = copyObject(objectOld);

    console.log(newObject);
